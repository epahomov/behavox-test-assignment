package com.behavox.channels

import org.apache.spark.graphx._

import org.apache.spark.rdd.RDD

object ChannelFinder {

  type PowerMultiplier = Double

  case class EdgeInformation(time: Timestamp, powerMultiplier: PowerMultiplier)

  val DEFAULT_MAX_ITERATIONS = 50
  val DEFAULT_POWER_DECRISING = 10

  def getStatistics(emails: RDD[Email], iterations: Int = DEFAULT_MAX_ITERATIONS, powerDecrising: Double = DEFAULT_POWER_DECRISING) = {
    val graph = createGraph(emails)
    processGraph(graph, iterations, powerDecrising).vertices.map(_._2)
      .map(emailStatistics =>
        emailStatistics.copy(senders = emailStatistics.senders.filter(p => !p._1.equals(emailStatistics.address)))    // delete myself from senders
      )
  }

  def createGraph(emails: RDD[Email]) = {

    val vertexes = emails
      .flatMap(email => email.to :+ email.from)
      .distinct()
      .map(address => (address.hashCode.toLong, EmailStatistics(address)))

    val edges = emails
      .flatMap(email =>
      email.to.map(to =>
        Edge(email.from.hashCode, to.hashCode, EdgeInformation(email.date, calculateEmailPower(email)))))

    Graph(vertexes, edges)

  }

  case class ChannelMessage(map: Map[EmailAddress, (Distance, Power)] = Map.empty)

  private def processGraph(graph: Graph[EmailStatistics, EdgeInformation], iterations: Int, powerDecrising: Double  ) = {
    Pregel.apply(graph, ChannelMessage(), iterations)(
      vprog,
      sendMessage(powerDecrising),
      mergeMessage
    )
  }

  private def vprog(id: VertexId, emailStatistics: EmailStatistics, message: ChannelMessage): EmailStatistics = {
    EmailStatistics(emailStatistics.address, mergeMap(emailStatistics.senders, message.map))
  }

  private def sendMessage(powerDecrising: Double)(triplet: EdgeTriplet[EmailStatistics, EdgeInformation]): Iterator[(VertexId, ChannelMessage)] = {
    Iterator((triplet.dstId,

      ChannelMessage(triplet.srcAttr.senders.map(senderStat => {
        (senderStat._1, (senderStat._2._1 + 1, (senderStat._2._2 / powerDecrising) * triplet.attr.powerMultiplier))
      }) + (triplet.srcAttr.address ->(1, 1 * triplet.attr.powerMultiplier))
      )

      ))
  }

  private def mergeMessage(message1: ChannelMessage, message2: ChannelMessage): ChannelMessage = {
    ChannelMessage(mergeMap(message1.map, message2.map))
  }

  def mergeMap(map1: Map[EmailAddress, (Distance, Power)], map2: Map[EmailAddress, (Distance, Power)]) = {
    (map1.toSeq ++ map2.toSeq)
      .groupBy(_._1)
      .map(emailInformation => {
      val distances = emailInformation._2.map(_._2._1)
      val powers = emailInformation._2.map(_._2._2)
      emailInformation._1 ->(distances.min, powers.sum)
    })
      .toMap
  }

  private def calculateEmailPower(email: Email) = {
    1.0 / Math.pow(email.to.size, 2)
  }

}
