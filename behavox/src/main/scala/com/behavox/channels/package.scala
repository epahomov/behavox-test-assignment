package com.behavox

package object channels {

  type Distance = Int
  type EmailAddress = String
  type Timestamp = Long
  type Power = Double

  case class Email(to: List[EmailAddress], from: EmailAddress, date: Timestamp, body: String)

  case class EmailStatistics(
                              address: EmailAddress,
                              senders: Map[EmailAddress, (Distance, Power)] = Map.empty)

}
