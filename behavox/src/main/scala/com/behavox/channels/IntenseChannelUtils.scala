package com.behavox.channels

import org.apache.spark.rdd.RDD

object IntenseChannelUtils {

  val TIME_DIFFER = 30 * 1000 * 60

  def filterEmailsFromIntenseChannels(emails: RDD[Email]): RDD[Email] = {
    emails
      .flatMap(email => email.to.map(to => email.copy(to = List(to))))
      .groupBy(email => email.from + email.to.head)
      .flatMap(compinedEmails => filterLocal(compinedEmails._2.toList))
  }

  private def filterLocal(emails: List[Email]) = {
    emails.filter(email => emails.count(comparedEmail => withInNMinutes(email, comparedEmail)) > 2)
  }

  private def withInNMinutes(email1: Email, email2: Email) = {
    email2.date > email1.date - TIME_DIFFER && email2.date < email1.date + TIME_DIFFER
  }

}
