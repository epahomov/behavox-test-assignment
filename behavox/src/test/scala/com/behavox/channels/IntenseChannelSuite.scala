package com.behavox.channels


class IntenseChannelSuite extends AbstractChannelTest {

  test("Simple broadcst less important then direct") {

    val input = List(
      Email(List("1@aaa.com"), "2@aaa.com", 1324223424L, ""),
      Email(List("1@aaa.com"), "2@aaa.com", 1324223425L, ""),
      Email(List("1@aaa.com"), "2@aaa.com", 1324223426L, ""),
      Email(List("1@aaa.com"), "3@aaa.com", 1324223423L, "")
    )

    val direct = process(IntenseChannelUtils.filterEmailsFromIntenseChannels(sc.parallelize(input)).collect().toList)

    checkDistance(direct, "1@aaa.com", "2@aaa.com", 1)
    checkDistance(direct, "1@aaa.com", "3@aaa.com", -1)

  }

}
