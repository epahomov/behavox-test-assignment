package com.behavox.channels

import org.scalatest.FunSuite

class ChannelFinderUnitSuite extends FunSuite {

  test("Test merge map function") {

    val map1 = Map(
      "a@b.com" ->(3, 5.0),
      "b@b.com" ->(0, 3.0),
      "c@b.com" ->(18, 4.0),
      "d@b.com" ->(3, 5.0)
    )
    val map2 = Map(
      "a@b.com" ->(3, 9.0),
      "b@b.com" ->(4, 6.0),
      "c@b.com" ->(7, 6.0)
    )
    val actual = ChannelFinder.mergeMap(map1, map2)

    val expected = Map(
      "a@b.com" ->(3, 14.0),
      "b@b.com" ->(0, 9.0),
      "c@b.com" ->(7, 10.0),
      "d@b.com" ->(3, 5.0)
    )

    assert(actual === expected) // Roman, Alex - Don't you want to move on scala? :)
  }

}
