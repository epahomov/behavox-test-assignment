package com.behavox.channels

import org.scalatest.FunSuite


class AbstractChannelTest extends FunSuite with LocalSparkContext {

  def process(emails: List[Email], iterations: Int = 10, powerDecrising: Double = 10.0) = {
    val rdd = sc.parallelize(emails)
    ChannelFinder
      .getStatistics(rdd, iterations, powerDecrising)
      .collect()
      .map(emailStat => emailStat.address -> emailStat)
      .toMap
  }

  def checkDistance(statistics: Map[EmailAddress, EmailStatistics], from: EmailAddress, to: EmailAddress, distance: Distance) = {
    statistics.get(from) match {
      case Some(emailStatitics) => {
        emailStatitics.senders.get(to) match {
          case None => assert(distance === -1)
          case Some(stat) => assert(stat._1 === distance)
        }
      }
      case None => assert(distance === -1)
    }
  }

}
