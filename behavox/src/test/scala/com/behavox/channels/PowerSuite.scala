package com.behavox.channels

class PowerSuite extends AbstractChannelTest {

  test("Simple broadcst less important then direct") {

    val directInput = List(
      Email(List("b@aaa.com"), "c@aaa.com", 1324223423L, "")
    )
    val direct = process(directInput)

    val broadcastInput = List(
      Email(List("b@aaa.com", "e@aaa.com"), "c@aaa.com", 1324223423L, "")
    )

    val broadcast = process(broadcastInput)

    assert(getPower("b@aaa.com", "c@aaa.com", direct) > getPower("b@aaa.com", "c@aaa.com", broadcast))

  }

  test("Distance affect power") {

    // 1 -> 2 -> 3

    val input = List(
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "3@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    assert(getPower("1@aaa.com", "2@aaa.com", result) > getPower("1@aaa.com", "3@aaa.com", result))

  }

  test("Mutliple mails affect power") {

    // 1
    //    -> -> 2
    //    ->    3

    val input = List(
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "3@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    assert(getPower("1@aaa.com", "2@aaa.com", result) > getPower("1@aaa.com", "3@aaa.com", result))

  }

  test("We find clusters") {

    // 1 -> <-  2     ->     5 -> <- 6 -> <- 9
    // | \    / |            |       |
    // 3 -> <-  4     ->     7 -> <- 8

    val input = List(
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "1@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "1@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "3@aaa.com", 1324223423L, ""), // 1 cluster
      Email(List("2@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "1@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "1@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "1@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "1@aaa.com", 1324223423L, ""),

      Email(List("5@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("5@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("5@aaa.com"), "7@aaa.com", 1324223423L, ""),
      Email(List("5@aaa.com"), "7@aaa.com", 1324223423L, ""),
      Email(List("5@aaa.com"), "8@aaa.com", 1324223423L, ""),
      Email(List("5@aaa.com"), "8@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "7@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "7@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "8@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "8@aaa.com", 1324223423L, ""), // second cluster
      Email(List("7@aaa.com"), "8@aaa.com", 1324223423L, ""),
      Email(List("7@aaa.com"), "8@aaa.com", 1324223423L, ""),
      Email(List("7@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("7@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("7@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("7@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("8@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("8@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("8@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("8@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("8@aaa.com"), "7@aaa.com", 1324223423L, ""),
      Email(List("8@aaa.com"), "7@aaa.com", 1324223423L, ""),

      Email(List("9@aaa.com"), "6@aaa.com", 1324223423L, ""),
      Email(List("6@aaa.com"), "9@aaa.com", 1324223423L, ""),


      Email(List("2@aaa.com"), "5@aaa.com", 1324223423L, ""),
      Email(List("5@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "7@aaa.com", 1324223423L, ""), // cluster communication
      Email(List("7@aaa.com"), "4@aaa.com", 1324223423L, "")

    )

    val result = process(input, 30, 100.0)

    assert(getPower("6@aaa.com", "9@aaa.com", result) > getPower("7@aaa.com", "9@aaa.com", result) * 2)
    assert(getPower("7@aaa.com", "9@aaa.com", result) > getPower("2@aaa.com", "9@aaa.com", result) * 5)
    assert(getPower("7@aaa.com", "9@aaa.com", result) > getPower("1@aaa.com", "9@aaa.com", result) * 10)

  }



  def getPower(from: EmailAddress, to: EmailAddress, statistics: Map[EmailAddress, EmailStatistics]) = {
    statistics.get(from).get.senders.get(to).get._2
  }


}
