package com.behavox.channels

class DistanceSuite extends AbstractChannelTest {

  test("Simple test") {

    val input = List(
      Email(List("b@aaa.com"), "c@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    checkDistance(result, "c@aaa.com", "b@aaa.com", -1)
    checkDistance(result, "b@aaa.com", "c@aaa.com", 1)
  }

  test("3 emails test") {

    val input = List(
      Email(List("b@aaa.com"), "c@aaa.com", 1324223423L, ""),
      Email(List("c@aaa.com"), "d@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    checkDistance(result, "c@aaa.com", "b@aaa.com", -1)
    checkDistance(result, "d@aaa.com", "c@aaa.com", -1)
    checkDistance(result, "b@aaa.com", "c@aaa.com", 1)
    checkDistance(result, "b@aaa.com", "d@aaa.com", 2)
  }

  test("2 routes test") {

    //     1 -> 2 -> 3 -> 4
    //       -----------> 4
    //

    val input = List(
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("1@aaa.com"), "4@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    checkDistance(result, "1@aaa.com", "4@aaa.com", 1)
  }


  test("cycle test") {

    //     1 -> 2
    //     |     |
    //     4 <- 3
    //

    val input = List(
      Email(List("1@aaa.com"), "2@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "4@aaa.com", 1324223423L, ""),
      Email(List("4@aaa.com"), "1@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    checkDistance(result, "1@aaa.com", "2@aaa.com", 1)
    checkDistance(result, "1@aaa.com", "3@aaa.com", 2)
    checkDistance(result, "1@aaa.com", "4@aaa.com", 3)
    checkDistance(result, "1@aaa.com", "1@aaa.com", -1)

    checkDistance(result, "3@aaa.com", "4@aaa.com", 1)
    checkDistance(result, "3@aaa.com", "1@aaa.com", 2)
    checkDistance(result, "3@aaa.com", "2@aaa.com", 3)
    checkDistance(result, "1@aaa.com", "1@aaa.com", -1)
  }

  test("both ways test") {

    //     1 <- 2 ->  <- 3
    //

    val input = List(
      Email(List("2@aaa.com"), "1@aaa.com", 1324223423L, ""),
      Email(List("2@aaa.com"), "3@aaa.com", 1324223423L, ""),
      Email(List("3@aaa.com"), "2@aaa.com", 1324223423L, "")
    )
    val result = process(input)

    checkDistance(result, "1@aaa.com", "2@aaa.com", -1)
    checkDistance(result, "1@aaa.com", "3@aaa.com", -1)

    checkDistance(result, "2@aaa.com", "1@aaa.com", 1)
    checkDistance(result, "2@aaa.com", "3@aaa.com", 1)

    checkDistance(result, "3@aaa.com", "1@aaa.com", 2)
    checkDistance(result, "3@aaa.com", "2@aaa.com", 1)
  }



}
